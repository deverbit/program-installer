const path = require('path')
const fs = require('fs')

const dir = path.resolve(__dirname, '..')
const file = dir + '/install.log.txt'

exports.create = function create(name) {
  return async function log(message, clear) {
    if (clear) fs.writeFileSync(file, '')
    fs.appendFileSync(
      file,
      `[${new Date().toISOString()}] ${name} : ${message}\n`
    )
  }
}
