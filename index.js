const chokidar = require('chokidar')
const path = require('path')
const glob = require('glob')
const fs = require('fs')
const _debounce = require('lodash.debounce')
const logger = require('./logger')
const installer = require('./installer')

const log = logger.create('main')
const dir = path.resolve(__dirname, '..')
const files = glob.sync(dir + '/install.*.json', { absolute: true })

async function start() {
  await log('start', true)
  chokidar
    .watch(files)
    .on(
      'add',
      (path) => {
        log(`watching install file ${path}`)
      }
    )
    .on(
      'change',
      _debounce(
        async (path) => {
          try {
            const contents = await new Promise((resolve, reject) => {
              fs.readFile(path, (err, data) => {
                if (err) return reject(err)
                resolve(data.toString())
              })
            })
            const config = JSON.parse(contents)
            log(`installing ${config.name}`)
            const error = await installer.install({ ...config, root: dir })
            log(`finished ${config.name}${(error && ' with error') || ''}`)
          } catch (error) {
            log(`${error.stack}`)
          }
        },
        1000 * 3
      )
    )
}

start()


