const path = require('path')
const fs = require('fs')
const glob = require('glob')
const del = require('del')
const tar = require('tar-fs')
const gunzip = require('gunzip-maybe')
const { exec } = require('child_process')
const logger = require('./logger')

const errors = {
  NO_LATEST_FILE: 'NO_LATEST_FILE'
}

class Installer {

  constructor(config) {
    this.log = logger.create(config.name)
    this.dir = config.root + config.path
    this.dirPackage = this.dir + '/package'
    this._packages = config.packages
    this._env = config.env || {}
  }

  async extract() {
    this.log(`prepare extract`)
    // delete previously extracted
    await del([this.dirPackage], { force: true })
    // get latest file to extract
    const files = glob.sync(this.dir + '/*.tgz', { absolute: true })
    let oldestFile = ''
    let oldestTime = Number.MAX_SAFE_INTEGER
    let latestFile = ''
    let latestTime = 0
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const ms = fs.statSync(file).birthtimeMs
      if (ms > latestTime) {
        latestTime = ms
        latestFile = file
      }
      if (ms <= oldestTime) {
        oldestTime = ms
        oldestFile = file
      }
    }
    // make sure a single file exists
    if (!latestFile) throw Error(errors.NO_LATEST_FILE)
    // start extracting
    this.log(`extracting ${path.basename(latestFile)}`)
    await new Promise((resolve, reject) => {
      fs.createReadStream(latestFile)
        .on('error', reject)
        .pipe(gunzip())
        .pipe(tar.extract(this.dir).on('finish', () => resolve()))
    })
    // delete the oldest file
    if (files.length > 3) {
      this.log(`removing old tgz`)
      await del([oldestFile], { force: true })
    }
    this.log(`extracted`)
  }

  async install() {
    // proceed install
    this.log(`prepare install`)
    // installing main and sub packages
    this.log(`installing ${this._packages.length} package(s)`)
    for (let i = 0; i < this._packages.length; i++) {
      const pkg = this._packages[i]
      const dir = path.resolve(this.dirPackage, pkg.path)
      const options = { cwd: dir, stdio: 'ignore' }
      this.log(`installing ${pkg.path}`)
      if (pkg.build) {
        await this._exec('NODE_ENV=development npm install', options)
        this.log(`building`)
        await this._exec('NODE_ENV=production npm run build', options)
        this.log(`pruning`)
        await this._exec('NODE_ENV=production npm prune', options)
      } else {
        await this._exec('npm ci --production', options)
      }
      this.log(`installed`)
    }
    this.log(`all installed`)
  }

  async _exec(command, options) {
    return new Promise((resolve, reject) => {
      exec(command, options, err => {
        if (err) return reject(err)
        resolve()
      })
    })
  }

}

exports.install = async function _install(config) {
  const installer = new Installer(config)
  installer.log(`package ${installer.dirPackage}`)
  try {
    // extract zip file first
    await installer.extract()
    // then install
    await installer.install()
  } catch (error) {
    installer.log(`error ${error.message}`)
    return true
  }
}
